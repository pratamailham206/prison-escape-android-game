﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class thingsScript : MonoBehaviour
{
    // Start is called before the first frame update
    public static int benda;
    public static int playerScore;
    public static float times;

    public Text txt;
    public Text timeText;

    void Start()
    {
        benda = 0;
        playerScore = 0;
    }

    // Update is called once per frame
    void Update()
    {
        times += Time.deltaTime;
        timeText.GetComponent<Text>().text = times.ToString();
        txt.GetComponent<Text>().text = benda.ToString();
    }
}
