﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStatusScript : MonoBehaviour
{
    // Start is called before the first frame update
    public Image playerHealth;
    public Image playerMana;

    void Start()
    {
        GetComponent<DragToMove>();        
    }

    // Update is called once per frame
    void Update()
    {
        playerHealth.fillAmount = DragToMove.health / 100;
        playerMana.fillAmount = DragToMove.mana / 50;

    }
}
