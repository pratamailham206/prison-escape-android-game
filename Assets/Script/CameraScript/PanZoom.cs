﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanZoom : MonoBehaviour
{
    private Func<Vector3> GetcameraFollowPosition;
    //public Vector2 panLimit;
    //public Vector2 minPanLimit;

    public void Setup(Func<Vector3> GetcameraFollowPosition)
    {
        this.GetcameraFollowPosition = GetcameraFollowPosition;
    }

    public void SetGetCameraFollowPos(Func<Vector3> GetCameraFollowPosFunc)
    {
        GetcameraFollowPosition = GetCameraFollowPosFunc;
    }
    
    void Update()
    {
        Vector3 cameraFollowPosition = GetcameraFollowPosition();
        cameraFollowPosition.z = transform.position.z;

        Vector3 cameraMoveDir = (cameraFollowPosition - transform.position).normalized;
        float distance = Vector3.Distance(cameraFollowPosition, transform.position);
        float cameraMoveSpeed = 5f;

        if(distance > 0)
        {
            Vector3 newCameraPos = transform.position + cameraMoveDir * distance * cameraMoveSpeed * Time.deltaTime;

            float distanceAfterMoving = Vector3.Distance(newCameraPos, cameraFollowPosition);

            if(distanceAfterMoving > distance)
            {
                newCameraPos = cameraFollowPosition;
            }

            transform.position = newCameraPos;
            //transform.position = new Vector3(Mathf.Clamp(transform.position.x, -minPanLimit.x, panLimit.x), Mathf.Clamp(transform.position.y, -minPanLimit.y, panLimit.y), -10);
        }
    }

    public IEnumerator Shake(float duration, float magnitude, Transform pos)
    {
        Vector3 originalPOs = this.transform.position;
        float elapsed = 0.0f;

        while(elapsed < duration)
        {
            float x = pos.position.x + UnityEngine.Random.Range(0.3f, 0.5f) * magnitude;
            float y = pos.position.y + UnityEngine.Random.Range(0.3f, 0.5f) * magnitude;

            transform.position = new Vector3(x, y, originalPOs.z);
            elapsed += Time.deltaTime;

            yield return null;
        }

        transform.localPosition = originalPOs;
    }
}
