﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MoveSceneScript : MonoBehaviour
{
    public string sceneLoaded;

    public void TargetMoveScene()
    {
        SceneManager.LoadScene(sceneLoaded);
    }

    public void PlayScene()
    {
        //ASync Move Scene
    }

    public void BackScene()
    {
        SceneManager.LoadScene("MainMenuScene");
    }
}
