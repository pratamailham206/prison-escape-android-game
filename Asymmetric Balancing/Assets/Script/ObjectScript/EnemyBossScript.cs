﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBossScript : MonoBehaviour
{
    //STATUS
    private Transform player;
    public float health;
    public float attack;
    public float speed;

    //MOVING
    public float stoppingDistance;
    public float retreatDistance;
    bool decrement;
    Vector3 tempAnim;

    //PATROL
    private float waitTime;
    public float startWaitTime;
    public float followingPlayer;
    public Transform[] moveSpots;
    private int spot;

    //ANIMATION
    public Animator anim;

    //ATTACK
    public LayerMask whatIsEnemies;
    public Transform attackPos;
    public float attackRange;
    Collider2D[] enemiesToDamage;

    void Start()
    {
        waitTime = startWaitTime;
        player = GameObject.FindGameObjectWithTag("Player").transform;
        spot = 0;
        health = 100;
        GetComponent<thingsScript>();
        decrement = false;
        tempAnim = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position += Vector3.left * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += Vector3.right * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += Vector3.up * speed * Time.deltaTime;
        }
        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
        */

        
        if (health <= 0)
        {
            EnemyDestroyed();
        }        

        if (Vector2.Distance(transform.position, player.position) <= followingPlayer)
        {
            if (Vector2.Distance(transform.position, player.position) > stoppingDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, speed * Time.deltaTime);
            }

            else if (Vector2.Distance(transform.position, player.position) < stoppingDistance && Vector2.Distance(transform.position, player.position) > retreatDistance)
            {
                transform.position = this.transform.position;
            }

            else if (Vector2.Distance(transform.position, player.position) < retreatDistance)
            {
                transform.position = Vector2.MoveTowards(transform.position, player.position, -speed * Time.deltaTime);
            }
            //ATTACK
            enemiesToDamage = Physics2D.OverlapCircleAll(attackPos.position, attackRange, whatIsEnemies);

            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                enemiesToDamage[i].GetComponent<DragToMove>().TakeDamage(attack);
            }
            //~~~ATTACK
        }

        else
        {
            transform.position = Vector2.MoveTowards(transform.position, moveSpots[spot].position, speed * Time.deltaTime);
            if (Vector2.Distance(transform.position, moveSpots[spot].position) < 0.2f)
            {
                if (waitTime <= 0)
                {
                    
                    if(spot <= 0)
                    {
                        decrement = false;
                    }

                    else if(spot >= moveSpots.Length-1)
                    {
                        decrement = true;
                    }

                    if (decrement)
                    {
                        spot--;
                    }

                    else if (decrement == false)
                    {
                        spot++;
                    }

                    waitTime = startWaitTime;
                }
                else
                {
                    waitTime -= Time.deltaTime;
                    tempAnim = transform.position;
                }
            }
            changeAnim(tempAnim - transform.position);
            tempAnim = transform.position;
        }
        
    }

    private void setAnim(Vector2 setVector)
    {
        anim.SetFloat("direction", setVector.x);
        anim.SetFloat("speed", speed);
    }

    private void changeAnim(Vector2 direction)
    {
        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            //RIGHT
            if (direction.x > 0)
            {
                setAnim(Vector2.left);
            }
            //LEFT
            else if (direction.x < 0)
            {
                setAnim(Vector2.right);
            }
        }

        else if (Mathf.Abs(direction.x) < Mathf.Abs(direction.y))
        {
            //UP
            if (direction.y > 0)
            {
                setAnim(Vector2.right);
            }
            //DOWN
            else if (direction.y < 0)
            {
                setAnim(Vector2.right);
            }
        }
    }
    public void EnemyDestroyed()
    {
        Destroy(this.gameObject);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            Debug.Log("Enemy win");
        }
    }
}
